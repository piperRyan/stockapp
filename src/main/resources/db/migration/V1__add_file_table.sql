CREATE TABLE IF NOT EXISTS file (
    id char(36) PRIMARY KEY,
    status varchar(20),
    error varchar(400)
)