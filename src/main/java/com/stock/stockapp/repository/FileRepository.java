package com.stock.stockapp.repository;

import com.stock.stockapp.entities.DataFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<DataFile, String> {


}
