package com.stock.stockapp.entities;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class DowJones {

    @Id
    @SequenceGenerator(name = "dow_jones_gen", sequenceName = "dow_jones_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dow_jones_gen")
    private Long id;
    
    private String quarter;
    
    private String stock;
    
    private String date;
    
    private String open;
    
    private String high;
    
    private String low;
    
    private String close;
    
    private String volume;
    
    private String percentChangePrice;
    
    private String percentChangeVolumeOverLastWk;
    
    private String previousWeeksVolume;
    
    private String nextWeeksOpen;
    
    private String nextWeeksClose;
    
    private String percentChangeNextWeeksPrice;
    
    private String daysToNextDividend;
    
    private String percentReturnNextDividend;
}
