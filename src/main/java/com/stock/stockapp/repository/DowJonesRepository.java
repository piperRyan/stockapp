package com.stock.stockapp.repository;

import com.stock.stockapp.entities.DowJones;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DowJonesRepository extends JpaRepository<DowJones, Long> {

    List<DowJones> findByStock(String stock);

}