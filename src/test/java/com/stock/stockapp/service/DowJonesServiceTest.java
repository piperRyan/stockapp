package com.stock.stockapp.service;

import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.entities.DowJones;
import com.stock.stockapp.repository.DowJonesRepository;
import com.stock.stockapp.transformer.DowJonesMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DowJonesServiceTest {

    @Mock
    private DowJonesRepository dowJonesRepository;
    @Mock
    private DowJonesMapper dowJonesMapper;
    private DowJonesService service;

    @BeforeEach
    void setUp() {
        service = new DowJonesService(dowJonesRepository, dowJonesMapper);
    }

    @Nested
    @DisplayName("Given the dow jones is being queried for")
    class GivenTheDowJonesIsBeingQueriedFor {

        @Nested
        @DisplayName("When the entries exist")
        class WhenTheEntriesExist {

            @BeforeEach
            void setUp() {
                when(dowJonesRepository.findByStock("AA")).thenReturn(List.of(DowJones.builder().stock("AA").build()));
                when(dowJonesMapper.entityToDto(any(DowJones.class))).thenReturn(DowJonesDto.builder().stock("AA").build());

            }

            @DisplayName("should return those entries")
            @Test
            void shouldReturnThoseEntries() {
                var actual = service.getStockData("AA");
                assertEquals("AA", actual.get(0).getStock());
            }
        }

        @Nested
        @DisplayName("When the entries don't exist")
        class WhenTheEntriesDontExist {

            @BeforeEach
            void setUp() {
                when(dowJonesRepository.findByStock("AA")).thenReturn(Collections.emptyList());
            }

            @DisplayName("should return a empty list")
            @Test
            void shouldReturnAEmptyList() {
                var actual = service.getStockData("AA");
                assertTrue(actual.isEmpty());
            }
        }

    }

    @Nested
    @DisplayName("Given a single dow jones is being added")
    class GivenASingleDowJonesIsBeingAdded {

        @BeforeEach
        void setUp() {
            doReturn(DowJones.builder().id(1l).build()).when(dowJonesRepository).save(any(DowJones.class));
            doReturn(DowJones.builder().stock("AA").build()).when(dowJonesMapper).dtoToEntity(any(DowJonesDto.class));
        }

        @DisplayName("should add the entry to the database")
        @Test
        void shouldAddTheEntryToTheDatabase() {
            service.addDowJonesRecord(DowJonesDto.builder().stock("AA").build());
            verify(dowJonesRepository).save(any(DowJones.class));
        }
    }
}