package com.stock.stockapp.transformer;

import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.entities.DowJones;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface DowJonesMapper {

   DowJones dtoToEntity(DowJonesDto dowJonesDto);

   DowJonesDto entityToDto(DowJones dowJones);
}
