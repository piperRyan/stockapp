package com.stock.stockapp.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.service.DowJonesService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/dowjones")
public class DowJonesController {

    private final DowJonesService dowJonesService;

    private final ObjectMapper objectMapper;

    @GetMapping("/{stockTicker}")
    @Cacheable("stocks")
    public List<DowJonesDto> getStockTickerData(@PathVariable String stockTicker) {
        return dowJonesService.getStockData(stockTicker);
    }

    @PostMapping
    @CacheEvict(value = "stocks", key = "#dowJones?.stock")
    @Scheduled(fixedRateString = "3600000")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveDowJones(@RequestBody @Valid DowJonesDto dowJones) {
        dowJonesService.addDowJonesRecord(dowJones);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<JsonNode> handleConstraintViolation(DataIntegrityViolationException exception) {
        var objectNode = objectMapper.createObjectNode();
        objectNode = objectNode.put("error", "The stock and the date must be unique!");
        return new ResponseEntity<>(objectNode, HttpStatus.BAD_REQUEST);
    }
}
