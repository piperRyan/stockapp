package com.stock.stockapp.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class DataFileDto {

    @Id
    private String id;

    @Column
    private String status;

    @Column
    private List<String> errors;
}
