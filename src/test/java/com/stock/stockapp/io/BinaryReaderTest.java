package com.stock.stockapp.io;

import org.junit.jupiter.api.*;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Given a series of bytes to read")
class BinaryReaderTest {

    private BinaryReader reader;

    @DisplayName("When there is a least one line of data")
    @Nested
    class WhenThereIsAtLeastOneLineOfData {

        @BeforeEach
        void setUp() {
            reader = new BinaryReader("test\n".getBytes(StandardCharsets.UTF_8));
        }

        @AfterEach
        void tearDown() {
            reader.close();
        }

        @DisplayName("it should read the line")
        @Test
        void isShouldReadTheLine() {
            var result = reader.readLine();
            assertTrue(result.isOk());
            assertEquals("test", result.getOk());
        }

        @DisplayName("it should return null when no lines are left")
        @Test
        void isShouldReturnEmptyWhenNoLinesAreLeft() {
            reader.readLine();
            var result = reader.readLine();
            assertTrue(result.isOk());
            assertNull(result.getOk());
        }
    }

}