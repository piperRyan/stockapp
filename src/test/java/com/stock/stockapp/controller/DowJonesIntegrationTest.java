package com.stock.stockapp.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.entities.DowJones;
import com.stock.stockapp.repository.DowJonesRepository;
import com.stock.stockapp.transformer.DowJonesMapper;
import com.stock.stockapp.transformer.DowJonesTransformer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Testcontainers
public class DowJonesIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    DowJonesRepository dowJonesRepository;

    @Autowired
    DowJonesTransformer transformer;

    @Autowired
    DowJonesMapper mapper;

    @Autowired
    CacheManager cacheManager;


    ObjectMapper objectMapper = new ObjectMapper();

    @Container
    private final static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres");

    @DynamicPropertySource
    static void registerPostgreProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", () -> postgres.getJdbcUrl());
        registry.add("spring.datasource.username", () -> postgres.getUsername());
        registry.add("spring.datasource.password", () -> postgres.getPassword());
    }

    @AfterEach
    void tearDown() {
        dowJonesRepository.deleteAll();
        cacheManager.getCache("stocks").clear();
    }

    @Nested
    @DisplayName("When the data exists to be queried")
    class WhenTheDataExistsToBeQueried {

        @DisplayName("should return a result")
        @Test
        void shouldReturnAResult() throws Exception {
            dowJonesRepository.save(DowJones.builder().stock("AA").date("10/07/2022").quarter("1").build());
            var actualBody = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dowjones/AA")).andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
            var arrNode = objectMapper.readValue(actualBody, ArrayNode.class);
            var arrIter = arrNode.iterator();
            while(arrIter.hasNext()) {
                JsonNode node = arrIter.next();
                assertEquals("AA", node.get("stock").asText());
                assertEquals("1", node.get("quarter").asText());
                assertEquals("10/07/2022", node.get("date").asText());
            }

        }
    }

    @Nested
    @DisplayName("When the data needs to be added")
    class WhenTheDataNeedsToBeAdded {

        @DisplayName("should return a result")
        @Test
        void shouldReturnAResult() throws Exception {
            var rawData = "1,AA,1/7/2011,$15.82,$16.72,$15.78,$16.42,239655616,3.79267,,,$16.71,$15.97,-4.42849,26,0.182704".split(",");
            var dowJones =  transformer.transform(rawData);
            var requestBody = objectMapper.writeValueAsString(dowJones);
            mockMvc.perform(MockMvcRequestBuilders.post("/v1/dowjones").content(requestBody).contentType("application/json")).andExpect(status().isCreated());
            var actualBody = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dowjones/AA")).andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
            var arrNode = objectMapper.readValue(actualBody, ArrayNode.class);
            var arrIter = arrNode.iterator();
            while(arrIter.hasNext()) {
                JsonNode node = arrIter.next();
                assertEquals("AA", node.get("stock").asText());
                assertEquals("1", node.get("quarter").asText());
                assertEquals("1/7/2011", node.get("date").asText());
            }

        }
    }
}
