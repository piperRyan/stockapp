package com.stock.stockapp.lang;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName("Given a result is created")
class ResultTest {

    @Nested
    @DisplayName("When the result is ok")
    class WhenTheResultIsOk {

        @DisplayName("it should be able to be created and marked as ok")
        @Test
        void itShouldBeAbleToBeCreatedAndMarkedAsOk() {
            assertTrue(Result.ok("test").isOk());
            assertFalse(Result.ok("test").isErr());
        }

        @DisplayName("it should be able to be created and get the ok value")
        @Test
        void itShouldBeAbleToBeCreatedAndGetTheOkValue() {
            assertEquals("test", Result.ok("test").getOk());
        }


        @DisplayName("it should be able to be created and mapped")
        @Test
        void itShouldBeAbleToBeCreatedAndMapped() {
            assertEquals(1, Result.ok("1").map(val -> Integer.valueOf((String) val)).getOk());
        }

        @DisplayName("it should be able to be created and flatMap")
        @Test
        void itShouldBeAbleToBeCreatedAndFlatMapped() {
            assertEquals(1, Result.ok("1").flatMap(val -> Result.ok(Integer.valueOf((String) val))).getOk());
        }

        @DisplayName("it should be able to be created and ifPresent should be run")
        @Test
        void itShouldBeAbleToBeCreatedAndIfPresentShouldBeRune() {
            var result = Result.ok("1");
            var iArr = new ArrayList<>();
            result.ifOkPresent(val -> Result.ok(iArr.add(val)));
            assertEquals("1", iArr.get(0));
        }

        @DisplayName("it should be able to be created and mapErr should do nothing")
        @Test
        void itShouldBeAbleToBeCreatedAndMapErrShouldMapTheErr() {
            assertEquals("1", Result.ok("1").mapErr(val -> Result.ok(Integer.valueOf((String) val))).getOk());
        }
    }

    @Nested
    @DisplayName("When the result is err")
    class WhenTheResultIsErr {
        @DisplayName("it should be able to be created and marked as ok")
        @Test
        void itShouldBeAbleToBeCreatedAndMarkedAsErr() {
            assertTrue(Result.err("test").isErr());
            assertFalse(Result.err("test").isOk());
        }


        @DisplayName("it should be able to be created and get the ok value")
        @Test
        void itShouldBeAbleToBeCreatedAndGetTheErrValue() {
            assertEquals("test", Result.err("test").getErr());
        }


        @DisplayName("it should be able to be created and map should do nothing")
        @Test
        void itShouldBeAbleToBeCreatedAndMapShouldDoNothing() {
            assertEquals("1", Result.err("1").map(val -> Integer.valueOf((String) val)).getErr());
        }

        @DisplayName("it should be able to be created and flatMap should do nothing")
        @Test
        void itShouldBeAbleToBeCreatedAndFlatMapShouldDoNothing() {
            assertEquals("1", Result.err("1").flatMap(val -> Result.ok(Integer.valueOf((String) val))).getErr());
        }


        @DisplayName("it should be able to be created and mapErr should map the err")
        @Test
        void itShouldBeAbleToBeCreatedAndMapErrShouldMapTheErr() {
            assertEquals(1, Result.err("1").mapErr(val -> Integer.valueOf((String) val)).getErr());
        }


        @DisplayName("it should be able to be created and ifErrPresent should be run")
        @Test
        void itShouldBeAbleToBeCreatedAndIfErrPresentShouldBeRun() {
            var result = Result.err("1");
            var iArr = new ArrayList<>();
            result.ifErrorPresent(val -> Result.ok(iArr.add(val)));
            assertEquals("1", iArr.get(0));
        }

        @Test
        void thing() {
            Result<Void, String> result = Result.ok(null);
            assertTrue(result.isOk());
        }

        @DisplayName("it should be able to be created and flatMap should do nothing")
        @Test
        void itShouldBeAbleToBeCreatedAndFlatMapped() {
            assertEquals("1", Result.err("1").flatMap(val -> Result.ok(Integer.valueOf((String) val))).getErr());
        }


    }



}