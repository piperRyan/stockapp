package com.stock.stockapp.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.stockapp.dto.DataFileDto;
import com.stock.stockapp.service.UploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/file")
public class FileController {

    private final UploadService uploadService;

    private final ObjectMapper objectMapper;

    @PostMapping
    public JsonNode uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        var fileUuid = UUID.randomUUID().toString();
        uploadService.uploadFile(fileUuid, file.getBytes());
        var objectNode = objectMapper.createObjectNode();
        objectNode = objectNode.put("fileId", fileUuid);
        return objectNode;
    }


    @GetMapping("/{uuid}")
    public DataFileDto getStatus(@PathVariable String uuid) {
        return uploadService.getFileUpload(uuid);
    }
}
