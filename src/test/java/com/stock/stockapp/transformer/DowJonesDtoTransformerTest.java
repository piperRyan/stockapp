package com.stock.stockapp.transformer;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({MockitoExtension.class})
@DisplayName("Given an object to transform into a dow jones")
class DowJonesDtoTransformerTest {

    private final DowJonesTransformer transformer = new DowJonesTransformer();


    @DisplayName("When the values are blank")
    @Nested
    class WhenTheValuesAreNull {

        @DisplayName("it should map the fields as null")
        @Test
        void itShouldMapTheFieldsAsNull() {
            var jones = transformer.transform(new String[]{"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""});
            assertNull(jones.getQuarter());
            assertNull(jones.getStock());
            assertNull(jones.getDate());
            assertNull(jones.getOpen());
            assertNull(jones.getHigh());
            assertNull(jones.getLow());
            assertNull(jones.getClose());
            assertNull(jones.getVolume());
            assertNull(jones.getPercentChangePrice());
            assertNull(jones.getPercentChangeVolumeOverLastWk());
            assertNull(jones.getPreviousWeeksVolume());
            assertNull(jones.getNextWeeksOpen());
            assertNull(jones.getNextWeeksClose());
            assertNull(jones.getPercentChangeNextWeeksPrice());
            assertNull(jones.getDaysToNextDividend());
            assertNull(jones.getPercentReturnNextDividend());
        }
    }

    @DisplayName("When the values are not blank")
    @Nested
    class WhenTheValuesAreNotBlank {

        @DisplayName("it should map the fields with the values")
        @Test
        void itShouldMapTheFieldsAsNull() {
            var jones = transformer.transform(new String[]{"1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1"});
            assertEquals("1", jones.getQuarter());
            assertEquals("1", jones.getStock());
            assertEquals("1", jones.getDate());
            assertEquals("1", jones.getOpen());
            assertEquals("1", jones.getHigh());
            assertEquals("1", jones.getLow());
            assertEquals("1", jones.getClose());
            assertEquals("1", jones.getVolume());
            assertEquals("1", jones.getPercentChangePrice());
            assertEquals("1", jones.getPercentChangeVolumeOverLastWk());
            assertEquals("1", jones.getPreviousWeeksVolume());
            assertEquals("1", jones.getNextWeeksOpen());
            assertEquals("1", jones.getNextWeeksClose());
            assertEquals("1", jones.getPercentChangeNextWeeksPrice());
            assertEquals("1", jones.getDaysToNextDividend());
            assertEquals("1", jones.getPercentReturnNextDividend());
        }
    }


}