package com.stock.stockapp.service;

import com.stock.stockapp.dto.DataFileDto;
import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.entities.DataFile;
import com.stock.stockapp.io.BinaryReader;
import com.stock.stockapp.lang.Result;
import com.stock.stockapp.repository.DowJonesRepository;
import com.stock.stockapp.repository.FileRepository;
import com.stock.stockapp.transformer.DowJonesMapper;
import com.stock.stockapp.transformer.DowJonesTransformer;
import com.stock.stockapp.validator.DowJonesValidator;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UploadService {

    private static final String IN_PROGRESS = "IN_PROGRESS";
    private static final String FAILED = "FAILED";
    private static final String SUCCESS = "SUCCESS";
    private final FileRepository fileRepository;
    private final CacheManager cacheManager;
    private final Validator validator;

    private final DowJonesRepository dowJonesRepository;

    private final DowJonesMapper dowJonesMapper;

    private final DowJonesTransformer dowJonesTransformer;

    private final DowJonesValidator dowJonesValidator;


    @Async
    public void uploadFile(String uuid, byte[] uploadedFile) {
        try {
            var fileAudit = DataFile.builder().id(uuid).status(IN_PROGRESS).build();
            fileRepository.save(fileAudit);
            var dowJones = intoDowJones(uploadedFile);
            dowJones.ifErrorPresent(err -> {
                fileAudit.setError(err);
                fileAudit.setStatus(FAILED);
                fileRepository.save(fileAudit);
            });
            dowJones.map(val -> val.stream().map(dowJone -> dowJonesMapper.dtoToEntity(dowJone)).collect(Collectors.toList())).ifOkPresent(dowJone -> {
                fileAudit.setStatus(SUCCESS);
                fileRepository.save(fileAudit);
                dowJonesRepository.saveAll(dowJone);
                cacheManager.getCache("stocks").clear();
            });
        } catch (DataIntegrityViolationException cvException) {
            var dataFile = DataFile.builder().id(uuid).status(FAILED).error("The stock and the date must be unique.").build();
            fileRepository.save(dataFile);
            log.error("{}", cvException);
        } catch (Exception e) {
            var dataFile = DataFile.builder().id(uuid).status(FAILED).error("Failed to retrieve the data from the uploaded file.").build();
            fileRepository.save(dataFile);
            log.error("{}", e);
        }
    }

    public DataFileDto getFileUpload(String uuid) {
        DataFile dataFile = fileRepository.findById(uuid).orElseGet(() -> DataFile.builder().id(uuid).status("UNKNOWN").error("Entry not found").build());
        List<String> errors = dataFile.getError() != null ? Arrays.asList((dataFile.getError().split("\n"))) : null;
        return DataFileDto.builder().id(uuid).status(dataFile.getStatus()).errors(errors).build();
    }

    private Result<List<DowJonesDto>, String> intoDowJones(byte[] fileData) {
        var records = new ArrayList<DowJonesDto>();
        boolean isFirstLine = true;
        var binaryReader = new BinaryReader(fileData);
        var lineRes = binaryReader.readLine();
        long row = 1;
        while (lineRes.isOk() && lineRes.getOk() != null) {
            String line = lineRes.getOk();
            if (isFirstLine) {
                isFirstLine = false;
                Result<String[], String> headerResult = dowJonesValidator.validateHeader(line);
                if (headerResult.isErr()) {
                    binaryReader.close();
                    return Result.err(headerResult.getErr());
                }
            } else {
                Result<DowJonesDto, String> dataResult = processDataValidation(line);
                if (dataResult.isErr()) {
                    binaryReader.close();
                    return Result.err("In row " + row + " " + dataResult.getErr());
                }
                dataResult.ifOkPresent(dowJones -> records.add(dowJones));
            }
            lineRes = binaryReader.readLine();
            row++;
        }
        var closeAction = binaryReader.close();
        return closeAction.map(val -> records);
    }

    private Result processDataValidation(String line) {
        Result<DowJonesDto, String> dowResult = dowJonesValidator.validateLength(line, "data")
                .map(val -> dowJonesTransformer.transform(val)).flatMap(dowJones -> {
                    Set<ConstraintViolation<DowJonesDto>> constraints = validator.validate(dowJones);
                    if (constraints.isEmpty()) {
                        return Result.ok(dowJones);
                    } else {
                        return Result.err(constraints);
                    }
                })
                .mapErr(err -> {
                    if (err instanceof Set constraints) {
                        List<String> collect = (List<String>) constraints.stream().map(constraint -> ((ConstraintViolation) constraint).getMessage()).collect(Collectors.toList());
                        var joinedErrMsgs = String.join("\n", collect);
                        return "Failed to upload file due to the following reasons: \n" + joinedErrMsgs;
                    }
                    return err;
                });
        return dowResult;
    }
}
