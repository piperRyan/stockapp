CREATE TABLE IF NOT EXISTS dow_jones (
    id bigint PRIMARY KEY,
    quarter char(1),
    stock varchar(10) not null,
    "date" varchar(20),
    open varchar(20),
    high varchar(20),
    low varchar(20),
    close varchar(20),
    volume varchar(20),
    percent_change_price varchar(40),
    percent_change_volume_over_last_wk varchar(40),
    previous_weeks_volume varchar(40),
    next_weeks_open varchar(40),
    next_weeks_close varchar(40),
    percent_change_next_weeks_price varchar(40),
    days_to_next_dividend varchar(40),
    percent_return_next_dividend varchar(40),
    CONSTRAINT unq_dow_jones UNIQUE (stock, "date")
);

CREATE SEQUENCE IF NOT EXISTS dow_jones_seq START 1;