# Stock App 

## Requirements
- Since this project leverages test containers docker is required. 
    - Not sure if this works with podman

## How to Run
- The most simple way to do this is to run `docker-compose up -d` and it will expost the application on port 10020. 
If this is a conflicting port you can change it in the docker-compose file. 

## Endpoints
- POST: /v1/file takes multipart file with the name of file 
- GET: /v1/file/{fileId} get the status of the file you upload 
- POST: /v1/dowjones/ Insert new DowJones Record as decribed in webpage
- GET /v1/dowjones/{stockTicker} query the data surrounding the stock. 

## Bulk Insert Work Flow 
- Post a file to /v1/file it will return a fileId
- Do a GET to /v1/file/{fileId} 
    - On success one will see the status is has been set to SUCCESS
    - If still being uploaded the status will be IN_PROGRESS
    - If error occurs status will be ERROR
        - the error field will be populated with the reasons why
    - Data should be avail /v1/dowjones/{stockTicker}

## Considerations
### Upload/Bulk Flow
Pros: 
- Upload flow was mainly taking into consideration the process of upload/inserting may exceed the http timeout
- Natural to leverage data format that original authors used as this may be a common way to share. 
Cons:
- Doesn't support different media types aside from the csv/"data" file that was provided in the website. This could
    be easily implemented for JSON and XML with minor refactoring. 
### Caching/Performance 
Pros: 
- Remove the need for excess db calls to query the stock with reasonable eviction processes. Thus making it fairly 
performant.
Cons:
- Not distributed caching with redis, but this can be easily refactored to be included.
- Better bulk/batch support for db (can adjust with property with hibernate to increase inserts, but other considerations are needed)  
### Type Support
Pros:
- Strings are awesome because you'll never have to deal with overflow/estimation errors! :) 
Cons: 
- Harder to post processing on it, but considering this app was more of a let's see the data coming back it was reasonable. Once again easily reactored
so it is good enough for prototyping.

## Would be nice
- CI/CD: Have 99% done just need to do more research on a odd daemon issue that is occuring in Ubuntu to allow test containers to work correctly, but
will automatically build and deploy to dockerhub with no issues. In anycase it's a more that's would be nice as test pass locally I think there more emphasis on the code itself.


