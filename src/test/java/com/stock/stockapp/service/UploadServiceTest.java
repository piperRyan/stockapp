package com.stock.stockapp.service;

import com.stock.stockapp.dto.DataFileDto;
import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.entities.DataFile;
import com.stock.stockapp.lang.Result;
import com.stock.stockapp.repository.DowJonesRepository;
import com.stock.stockapp.repository.FileRepository;
import com.stock.stockapp.transformer.DowJonesMapper;
import com.stock.stockapp.transformer.DowJonesTransformer;
import com.stock.stockapp.validator.DowJonesValidator;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.Data;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UploadServiceTest {

    @Mock
    private FileRepository fileRepository;

    @Mock
    private CacheManager cacheManager;

    @Mock
    private Validator validator;

    @Mock
    private DowJonesRepository dowJonesRepository;

    @Mock
    private DowJonesMapper dowJonesMapper;

    @Mock
    private DowJonesTransformer dowJonesTransformer;

    @Mock
    private DowJonesValidator dowJonesValidator;

    private UploadService uploadService;



    @BeforeEach
    void setUp() {
        uploadService = new UploadService(fileRepository, cacheManager, validator, dowJonesRepository, dowJonesMapper, dowJonesTransformer, dowJonesValidator);
    }

    @Nested
    @DisplayName("Given a file is uploaded")
    class  GivenAFileIsUploaded {
        @Nested
        @DisplayName("When it is a valid csv")
        class WhenItIsAValidCsv {
            @Mock
            Cache cache;
            private String fileData;

            @BeforeEach
            @SneakyThrows
            void setUp() {
                URI uri = getClass().getClassLoader().getResource("test_data/test.data").toURI();
                fileData = Files.readString(Paths.get(uri));
                doAnswer((i -> i.getArguments()[0])).when(fileRepository).save(any(DataFile.class));
                when(dowJonesTransformer.transform(any(String[].class))).thenReturn(DowJonesDto.builder().build());
                when(validator.validate(any(DowJonesDto.class))).thenReturn(Set.of());
                var headerValidatorReturn =  "quarter,stock,date,open,high,low,close,volume,percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend".split(",");
                var dataReturn = "1,AA,1/7/2011,$15.82,$16.72,$15.78,$16.42,239655616,3.79267,,,$16.71,$15.97,-4.42849,26,0.182704".split(",");
                when(dowJonesValidator.validateHeader(anyString())).thenReturn(Result.ok(headerValidatorReturn));
                when(dowJonesValidator.validateLength(anyString(), anyString())).thenReturn(Result.ok(dataReturn));
                when(cacheManager.getCache("stocks")).thenReturn(cache);
                doNothing().when(cache).clear();
            }

            @DisplayName("Should save the data to the dow jones table")
            @Test
            void itShouldSaveDataToTheDowJonesTable() {
                uploadService.uploadFile(UUID.randomUUID().toString(), fileData.getBytes());
                verify(fileRepository, times(2)).save(any(DataFile.class));
                verify(dowJonesRepository, times(1)).saveAll(anyList());
                verify(cacheManager).getCache("stocks");
                verify(cache).clear();
            }
        }

        @Nested
        @DisplayName("When it is a invalid csv")
        class WhenItIsAInValidCsv {
            @Mock
            private MultipartFile mFile;

            private String fileData;

            @BeforeEach
            @SneakyThrows
            void setUp() {
                URI uri = getClass().getClassLoader().getResource("test_data/test.data").toURI();
                fileData = Files.readString(Paths.get(uri));
                doAnswer((i -> i.getArguments()[0])).when(fileRepository).save(any(DataFile.class));
            }

            @Nested
            @DisplayName("And when the header validation fails")
            class AndWhenTheHeaderValidationFails {

                @BeforeEach
                void setUp() {
                    when(dowJonesValidator.validateHeader(anyString())).thenReturn(Result.err("Invalid header path"));

                }

                @DisplayName("Should save the data to the dow jones table")
                @Test
                void itShouldUpdateTheFileTable() {
                    uploadService.uploadFile(UUID.randomUUID().toString(), fileData.getBytes());
                    verify(fileRepository, times(2)).save(any(DataFile.class));
                }

            }

            @Nested
            @DisplayName("And when the length validation fails")
            class AndWhenTheLengthValidationFails {

                @BeforeEach
                void setUp() {
                    var headerValidatorReturn =  "quarter,stock,date,open,high,low,close,volume,percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend".split(",");
                    when(dowJonesValidator.validateHeader(anyString())).thenReturn(Result.ok(headerValidatorReturn));
                    when(dowJonesValidator.validateLength(anyString(), anyString())).thenReturn(Result.err("Invalid header path"));

                }

                @DisplayName("Should save the data to the dow jones table")
                @Test
                void itShouldUpdateTheFileTable() {
                    uploadService.uploadFile(UUID.randomUUID().toString(), fileData.getBytes());
                    verify(fileRepository, times(2)).save(any(DataFile.class));
                }

            }

            @Nested
            @DisplayName("And when the dto validation fails")
            class AndWhenTheDtoValidationFails {

                @Mock
                ConstraintViolation violation;

                @BeforeEach
                void setUp() {
                    var headerValidatorReturn =  "quarter,stock,date,open,high,low,close,volume,percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend".split(",");
                    var dataReturn = "1,AA,1/7/2011,$15.82,$16.72,$15.78,$16.42,239655616,3.79267,,,$16.71,$15.97,-4.42849,26,0.182704".split(",");
                    when(dowJonesValidator.validateHeader(anyString())).thenReturn(Result.ok(headerValidatorReturn));
                    when(dowJonesValidator.validateLength(anyString(), anyString())).thenReturn(Result.ok(dataReturn));
                    when(dowJonesTransformer.transform(any(String[].class))).thenReturn(DowJonesDto.builder().build());
                    when(violation.getMessage()).thenReturn("some error");
                    when(validator.validate(any(DowJonesDto.class), any())).thenReturn(Set.of(violation));
                }

                @DisplayName("Should update file table")
                @Test
                void itShouldUpdateTheFileTable() {
                    uploadService.uploadFile(UUID.randomUUID().toString(), fileData.getBytes());
                    verify(fileRepository, times(2)).save(any(DataFile.class));
                }

            }

        }

    }

    @Nested
    @DisplayName("Given a file has already been processed")
    class GivenAFileHasAlreadyBeenProcessed {

        @BeforeEach
        void setUp() {
            doAnswer(invocationOnMock -> Optional.of(DataFile.builder().error("test\ntest2").id((String)invocationOnMock.getArguments()[0]).status("ERROR").build())).when(fileRepository).findById(anyString());
        }

        @DisplayName("should be able to query it's status")
        @Test
        public void shouldBeAbleToQueryItsStatus() {
            String uuid = UUID.randomUUID().toString();
            DataFileDto actual = uploadService.getFileUpload(uuid);
            assertEquals("ERROR", actual.getStatus());
            assertEquals(uuid, actual.getId());
            assertEquals(2, actual.getErrors().size());
            assertEquals("test2", actual.getErrors().get(1));
        }
    }


}