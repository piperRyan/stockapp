package com.stock.stockapp.lang;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class Result<T,E> {

    private T val;
    private E error;

    private boolean isOk;


    private Result(T val, E error, boolean isOk){
        this.val = val;
        this.error = error;
        this.isOk = isOk;
    }

    public static <T, E> Result ok(T val) {
        return new Result<T,E>(val, null, true);
    }

    public static <T,E> Result err(E error) {
        return new Result<T,E>(null, error, false);
    }

    public <R> Result<R,E> flatMap(Function<? super T, Result<? extends  R, E>> mapper) {
        Objects.requireNonNull(mapper);
        if(val != null) {
             var result = mapper.apply(val);
             if(result.isOk()) {
                 return Result.ok(result.getOk());
             } else {
                 return Result.err(result.getErr());
             }
        }
        return Result.err(error);
    }

    public <R> Result<T,E> mapErr(Function<? super E, ? extends  R> mapper) {
        Objects.requireNonNull(mapper);
        if(error != null) {
            return Result.err(mapper.apply(error));
        }
        return Result.ok(val);
    }

    public <R> Result<R, E> map(Function<? super T, ? extends R> mapper) {
        Objects.requireNonNull(mapper);
        if(val != null) {
            return Result.ok(mapper.apply(val));
        }
        return Result.err(error);
    }

    public void ifErrorPresent(Consumer<E> action) {
        Objects.requireNonNull(action);
        if(error != null) {
            action.accept(error);
        }
    }



    public void ifOkPresent(Consumer<T> action) {
        Objects.requireNonNull(action);
        if(val != null) {
            action.accept(val);
        }
    }

    public boolean isErr() {
        return !isOk;
    }

    public E getErr() {
        return error;
    }

    public boolean isOk() {

        return isOk;
    }

    public T getOk() {
        return val;
    }


}
