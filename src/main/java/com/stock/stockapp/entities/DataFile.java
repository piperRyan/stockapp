package com.stock.stockapp.entities;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "file")
public class DataFile {

    @Id
    private String id;

    @Column
    private String status;

    @Column
    private String error;
}
