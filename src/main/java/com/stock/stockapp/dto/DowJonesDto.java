package com.stock.stockapp.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class DowJonesDto {
    @NotNull(message = "quater must be not null")
    @Size(min=1, max=1, message = "quater must be 1 or 2")
    @Pattern(regexp="[12]", message = "quater must be 1 or 2")
    private String quarter;
    @NotNull(message = "stock must be not null")
    @Size(min=1, max=5, message = "stock must be between 1 - 5  chars")
    private String stock;
    @Pattern(regexp = "^\\d{1,2}\\/\\d{1,2}\\/\\d{2,4}$", message = "date must have dd?/mm?/yy(yy)? format")
    private String date;
    @Pattern(regexp = "^\\$\\d{1,15}\\.\\d{1,2}", message = "open: Invalid monetary format")
    private String open;
    @Pattern(regexp = "^\\$\\d{1,15}\\.\\d{1,2}", message = "high: Invalid monetary format")
    private String high;
    @Pattern(regexp = "^\\$\\d{1,15}\\.\\d{1,2}", message = "low: Invalid monetary format")
    private String low;
    @Pattern(regexp = "^\\$\\d{1,15}\\.\\d{1,2}", message = "close: Invalid monetary format")
    private String close;
    @Pattern(regexp ="\\d{1,15}", message = "Volume must be a integer")
    private String volume;
    @Pattern(regexp ="-?\\d{1,4}(\\.\\d{1,20})?", message = "percent change price must be a decimal value")
    private String percentChangePrice;
    @Pattern(regexp ="-?\\d{1,4}(\\.\\d{1,20})?", message = "percent change volume over last week must be a decimal value")
    private String percentChangeVolumeOverLastWk;
    @Pattern(regexp ="\\d{1,15}", message = "previous week volume must be a integer value")
    private String previousWeeksVolume;
    @Pattern(regexp = "^\\$\\d{1,15}\\.\\d{1,2}", message = "nextWeeksOpen: Invalid monetary format")
    private String nextWeeksOpen;
    @Pattern(regexp = "^\\$\\d{1,15}\\.\\d{1,2}", message = "nextWeeksClose: Invalid monetary format")
    private String nextWeeksClose;
    @Pattern(regexp ="-?\\d{1,4}(\\.\\d{1,20})?", message = "next week price change must be a decimal value")
    private String percentChangeNextWeeksPrice;
    @Pattern(regexp ="\\d{1,6}", message = "Following days to next dividend must be a integer")
    private String daysToNextDividend;
    @Pattern(regexp ="-?\\d{1,4}(\\.\\d{1,20})?", message = "percent return must be a decimal value")
    private String percentReturnNextDividend;
}
