package com.stock.stockapp.transformer;

import com.stock.stockapp.dto.DowJonesDto;
import org.springframework.stereotype.Component;

@Component
public class DowJonesTransformer {

    public DowJonesDto transform(String[] val) {
        return DowJonesDto.builder().quarter(val[0].isBlank() == true ? null : val[0])
                .stock(val[1].isBlank() == true ? null :  val[1])
                .date(val[2].isBlank() == true ? null :  val[2])
                .open(val[3].isBlank() == true ? null :  val[3])
                .high(val[4].isBlank() == true ? null :  val[4])
                .low(val[5].isBlank() == true ? null :  val[5])
                .close(val[6].isBlank() == true ? null :  val[6])
                .volume(val[7].isBlank() == true ? null :  val[7])
                .percentChangePrice(val[8].isBlank() == true ? null :  val[8])
                .percentChangeVolumeOverLastWk(val[9].isBlank() == true ? null :  val[9])
                .previousWeeksVolume(val[10].isBlank() == true ? null : val[10])
                .nextWeeksOpen(val[11].isBlank() == true ? null: val[11])
                .nextWeeksClose(val[12].isBlank() == true ? null : val[12])
                .percentChangeNextWeeksPrice(val[13].isBlank() == true ? null : val[13])
                .daysToNextDividend(val[14].isBlank() == true ? null : val[14])
                .percentReturnNextDividend(val[15].isBlank() == true ? null : val[15]).build();

    }
}
