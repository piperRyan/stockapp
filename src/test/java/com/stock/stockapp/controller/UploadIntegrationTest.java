package com.stock.stockapp.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.stockapp.config.AsyncConfig;
import com.stock.stockapp.entities.DataFile;
import com.stock.stockapp.entities.DowJones;
import com.stock.stockapp.repository.DowJonesRepository;
import com.stock.stockapp.repository.FileRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@Import(AsyncConfig.class)
@SpringBootTest
@AutoConfigureMockMvc
@Testcontainers

public class UploadIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    DowJonesRepository dowJonesRepository;

    ObjectMapper objectMapper = new ObjectMapper();

    @Container
    private final static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres");

    @DynamicPropertySource
    static void registerPostgreProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", () -> postgres.getJdbcUrl());
        registry.add("spring.datasource.username", () -> postgres.getUsername());
        registry.add("spring.datasource.password", () -> postgres.getPassword());
    }

    @AfterEach
    void tearDown() {
        fileRepository.deleteAll();
        dowJonesRepository.deleteAll();
    }

    @Test
    @SneakyThrows
    @DisplayName("Should immediately return a file id")
    void shouldImmediatelyReturnAFileId() {
        File testFile = readFile("test_data/test.data");
        FileInputStream fileStream = new FileInputStream(testFile);
        MockMultipartFile stockData = new MockMultipartFile("file", testFile.getName(), "multipart/form-data", fileStream);
        MockMultipartHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.multipart("/v1/file")
                .file(stockData);
        String response = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        JsonNode node = objectMapper.readValue(response, JsonNode.class);
        assertTrue(node.get("fileId").asText().matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"));
    }


    @Test
    @SneakyThrows
    @DisplayName("Should update the file repostiory")
    void shouldUpdateTheFileRepository() {
        File testFile = readFile("test_data/test.data");
        FileInputStream fileStream = new FileInputStream(testFile);
        MockMultipartFile stockData = new MockMultipartFile("file", testFile.getName(), "multipart/form-data", fileStream);
        MockMultipartHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.multipart("/v1/file")
                .file(stockData);
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
        List<DataFile> dFiles = fileRepository.findAll();
        assertEquals(1, dFiles.size());
        assertEquals("SUCCESS", dFiles.get(0).getStatus());
    }

    @Test
    @SneakyThrows
    @DisplayName("Should update the dow jones repostiory")
    void shouldUpdateTheDowJonesRepository() {
        File testFile = readFile("test_data/test.data");
        FileInputStream fileStream = new FileInputStream(testFile);
        MockMultipartFile stockData = new MockMultipartFile("file", testFile.getName(), "multipart/form-data", fileStream);
        MockMultipartHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.multipart("/v1/file")
                .file(stockData);
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
        List<DowJones> dowJones = dowJonesRepository.findAll();
        assertEquals(1, dowJones.size());
        assertEquals("AA", dowJones.get(0).getStock());
    }

    @Test
    @SneakyThrows
    @DisplayName("should be able to query the file repository")
    void shouldBeAbleToQueryTheFileRepository() {
        String uuid = UUID.randomUUID().toString();
        var file = DataFile.builder().id(uuid).status("ERROR").error("test\ntest2").build();
        fileRepository.save(file);
        String body = mockMvc.perform(MockMvcRequestBuilders.get("/v1/file/" + uuid)).andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        JsonNode node = objectMapper.readValue(body, JsonNode.class);
        assertEquals("ERROR", node.get("status").asText());
    }

    private File readFile(String path) throws IOException, URISyntaxException {
        URI uri = getClass().getClassLoader().getResource(path).toURI();
        return new File(uri);
    }


}
