package com.stock.stockapp.io;

import com.stock.stockapp.lang.Result;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

@Slf4j
public class BinaryReader {

    private BufferedReader reader;

    public BinaryReader(byte [] data) {
        reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(data)));
    }



    public Result<String, String> readLine() {
        try {
            return Result.ok(this.reader.readLine());
        } catch (Exception e) {
            log.error("{}", e);
            return Result.err("Failed to read new line.");
        }
    }

    public Result<String, String> close() {
        try {
            this.reader.close();
            // using an empty string to represent an "empty" type.
            return Result.ok("");
        } catch (Exception e) {
            log.error("{}", e);
            return Result.err("Failed to close file.");
        }
    }
}
