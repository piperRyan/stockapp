package com.stock.stockapp.validator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Given a string array that represent a dow jones object")
class DowJonesDtoValidatorTest {

    private DowJonesValidator validator = new DowJonesValidator();

    @ParameterizedTest
    @MethodSource("headers")
    @DisplayName("it should validate the headers")
    void itShouldValidateTheHeaders(String headers, boolean isValid) {
        var result = validator.validateHeader(headers);
        assertEquals(result.isOk(), isValid);
    }

    private static Stream<Arguments> headers() {
        return Stream.of(
                Arguments.of("quarter,stock,date,open,high,low,close,volume,percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend", true),
                Arguments.of("quarter,stock,date,open,high,low,close,volume,percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividesd", false),
                Arguments.of("", false),
                Arguments.of(null, false),
                Arguments.of("sdf", false)
        );
    }

    private static Stream<Arguments> dowValidator() {
        return Stream.of(
                Arguments.of("quarter,stock,date,open,high,low,close,volume,percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend", true),
                Arguments.of("quarter,stock,date,open,high,low,close,volume,percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividesd", false),
                Arguments.of("", false),
                Arguments.of(null, false),
                Arguments.of("sdf", false)
        );
    }


}