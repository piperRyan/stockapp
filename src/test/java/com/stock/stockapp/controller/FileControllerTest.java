package com.stock.stockapp.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.stockapp.dto.DataFileDto;
import com.stock.stockapp.service.UploadService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
@DisplayName("Given a post request is made")
class FileControllerTest {

    @Mock
    UploadService service;

    @Mock
    MultipartFile multipartFile;

    FileController fileController;

    @BeforeEach
    void setUp() {
        fileController = new FileController(service, new ObjectMapper());
    }


    @DisplayName("should call the upload service and return a uuid")
    @Test
    void shouldCallTheUploadService() throws IOException {
        doNothing().when(service).uploadFile(anyString(), eq("test".getBytes()));
        when(multipartFile.getBytes()).thenReturn("test".getBytes());
        JsonNode result = fileController.uploadFile(multipartFile);
        verify(service, times(1)).uploadFile(anyString(), eq("test".getBytes()));
        assertTrue(result.get("fileId").asText().matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"));
    }

    @DisplayName("should return info a file has been uploaded already")
    @Test
    void shouldReturnInfoAFileHasBeenUploadedAlready() {
        String uuid = UUID.randomUUID().toString();
        doAnswer(invocationOnMock -> DataFileDto.builder().id(uuid).status("SUCCESS").errors(null).build()).when(service).getFileUpload(uuid);
        DataFileDto actual = fileController.getStatus(uuid);
        verify(service, times(1)).getFileUpload(uuid);
        assertEquals(uuid, actual.getId());
        assertEquals("SUCCESS", actual.getStatus());
        assertEquals(null, actual.getErrors());
    }

}