package com.stock.stockapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.service.DowJonesService;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DowJonesControllerTest {

    @Mock
    private DowJonesService dowJonesService;

    private DowJonesController controller;

    @BeforeEach
    void setUp() {
        controller = new DowJonesController(dowJonesService, new ObjectMapper());
    }

    @Nested
    @DisplayName("Given the dow jones needs to be queried")
    class GivenTheDowJonesNeedsToBeQueried {

        @BeforeEach
        void setUp() {
            when(dowJonesService.getStockData("AA")).thenReturn(List.of(DowJonesDto.builder().stock("AA").build()));
        }

        @DisplayName("should return the dow jones")
        @Test
        public void shouldReturnTheDowJones() {
            var actual = controller.getStockTickerData("AA");
            assertEquals("AA", actual.get(0).getStock());
            verify(dowJonesService, times(1)).getStockData("AA");

        }

    }



    @Nested
    @DisplayName("Given a dow jones needs to be saved")
    class GivenTheDowJonesNeedsToBeSaved {

        @Nested
        @DisplayName("When the inputted data is valid")
        class WhenTheInputtedDataIsValid {
            @BeforeEach
            void setUp() {
                doNothing().when(dowJonesService).addDowJonesRecord(any(DowJonesDto.class));
            }

            @DisplayName("should save the dow jones")
            @Test
            public void shouldReturnTheDowJones() {
                controller.saveDowJones(DowJonesDto.builder().build());
                verify(dowJonesService, times(1)).addDowJonesRecord(any(DowJonesDto.class));
            }
        }

        @Nested
        @DisplayName("When the inputted data is not valid")
        class WhenTheInputtedDataIsNotValid {

            @DisplayName("should handle constraint violations")
            @Test
            public void shouldHandleConstraintViolations() {
                var actual = controller.handleConstraintViolation(new DataIntegrityViolationException("test", null));
                var body = actual.getBody();
                assertEquals("The stock and the date must be unique!", body.get("error").asText());
            }
        }
    }
}