package com.stock.stockapp.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.stock.stockapp.dto.DowJonesDto;
import com.stock.stockapp.entities.DowJones;
import com.stock.stockapp.repository.DowJonesRepository;
import com.stock.stockapp.transformer.DowJonesMapper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class DowJonesService {

    private final DowJonesRepository dowJonesRepository;

    private  final DowJonesMapper mapper;

    public List<DowJonesDto> getStockData(String stockTicker) {
        List<DowJones> dowJones = dowJonesRepository.findByStock(stockTicker);
        return dowJones.stream().map(dJones -> mapper.entityToDto(dJones)).collect(Collectors.toList());
    }

    @PostMapping
    public void addDowJonesRecord(DowJonesDto dowJones) {
        var dowJonesEntity = mapper.dtoToEntity(dowJones);
        dowJonesRepository.save(dowJonesEntity);
    }
}
