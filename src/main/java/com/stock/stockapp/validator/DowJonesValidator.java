package com.stock.stockapp.validator;

import com.stock.stockapp.lang.Result;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DowJonesValidator {

    private static final String[] COLUMN_NAMES = {
            "quarter",
            "stock",
            "date",
            "open",
            "high",
            "low",
            "close",
            "volume",
            "percent_change_price",
            "percent_change_volume_over_last_wk",
            "previous_weeks_volume",
            "next_weeks_open",
            "next_weeks_close",
            "percent_change_next_weeks_price",
            "days_to_next_dividend",
            "percent_return_next_dividend"
    };

    public Result<String[], String> validateHeader(String header) {
        var result = validateLength(header, "header");
        return result.flatMap(headerFields -> {
            for (int i = 0; i < headerFields.length; i++) {
                if (!headerFields[i].equals(COLUMN_NAMES[i])) {
                    return Result.err("Invalid header: " + headerFields[i] + " was expecting: " + COLUMN_NAMES[i]);
                }
            }
             return Result.ok(headerFields);
        });

    }

    public Result<String[], String> validateLength(String val, String fieldType) {
        return Optional.ofNullable(val)
                .map(str -> str.split(","))
                .filter(str -> str.length == COLUMN_NAMES.length)
                .map(str -> Result.ok(str))
                .orElseGet(() -> Result.err("Incorrect amount of " + fieldType + " fields were found. Expecting " + COLUMN_NAMES.length + " but found a different amount"));
    }
}
